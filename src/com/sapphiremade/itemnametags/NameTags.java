package com.sapphiremade.itemnametags;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

@SuppressWarnings("deprecation")
public class NameTags {
	
	public ItemStack itemNameTag() {
		
		ItemStack is = new ItemStack(Material.getMaterial(Core.get.getConfig().getInt("ItemNameTag.Item.ItemID")), 1, (short) Core.get.getConfig().getInt("ItemNameTag.Item.ItemMeta"));
		ItemMeta ismeta = is.getItemMeta();
		
		ismeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Core.get.getConfig().getString("ItemNameTag.Item.Name")));
		ArrayList<String> islore = new ArrayList<String>();
		
		for (String allLore : Core.get.getConfig().getStringList("ItemNameTag.Item.Lore")) {
			
			islore.add(ChatColor.translateAlternateColorCodes('&', allLore));
		}
		
		ismeta.setLore(islore);
		is.setItemMeta(ismeta);
		
		return is;
	}
	
	public ItemStack itemLoreTag() {
		
		ItemStack is = new ItemStack(Material.getMaterial(Core.get.getConfig().getInt("ItemLoreTag.Item.ItemID")), 1, (short) Core.get.getConfig().getInt("ItemLoreTag.Item.ItemMeta"));
		ItemMeta ismeta = is.getItemMeta();
		
		ismeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Core.get.getConfig().getString("ItemLoreTag.Item.Name")));
		ArrayList<String> islore = new ArrayList<String>();
		
		for (String allLore : Core.get.getConfig().getStringList("ItemLoreTag.Item.Lore")) {
			
			islore.add(ChatColor.translateAlternateColorCodes('&', allLore));
		}
		
		ismeta.setLore(islore);
		is.setItemMeta(ismeta);
		
		return is;
	}
	
	public ItemStack loreRemoveTag() {
		
		ItemStack is = new ItemStack(Material.getMaterial(Core.get.getConfig().getInt("LoreRemoveTag.Item.ItemID")), 1, (short) Core.get.getConfig().getInt("LoreRemoveTag.Item.ItemMeta"));
		ItemMeta ismeta = is.getItemMeta();
		
		ismeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Core.get.getConfig().getString("LoreRemoveTag.Item.Name")));
		ArrayList<String> islore = new ArrayList<String>();
		
		for (String allLore : Core.get.getConfig().getStringList("LoreRemoveTag.Item.Lore")) {
			
			islore.add(ChatColor.translateAlternateColorCodes('&', allLore));
		}
		
		ismeta.setLore(islore);
		is.setItemMeta(ismeta);
		
		return is;
	}
}
