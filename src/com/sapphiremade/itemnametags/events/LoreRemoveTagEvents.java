package com.sapphiremade.itemnametags.events;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.sapphiremade.itemnametags.Core;
import com.sapphiremade.itemnametags.NameTags;

@SuppressWarnings("deprecation")
public class LoreRemoveTagEvents implements Listener {

	NameTags tags = new NameTags();

	@EventHandler
	public void onItemLoreTagUse(PlayerInteractEvent e) {

		Player p = e.getPlayer();

		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {

			if (p.getItemInHand().isSimilar(tags.loreRemoveTag())) {

				if (Core.get.usingItemLoreTag.contains(p) || Core.get.usingItemNameTag.contains(p) || Core.get.usingLoreRemoveTag.contains(p)) {

					for (String all : Core.get.getConfig().getStringList("ItemLoreTag.Messages.AlreadyNaming")) {

						p.sendMessage(ChatColor.translateAlternateColorCodes('&', all));
					}

				} else {

					if (p.getItemInHand().getType() == Material.AIR || p.getItemInHand().getType() == null || p.getItemInHand() == null) {

						for (String all : Core.get.getConfig().getStringList("ItemNameTag.Messages.Air")) {

							p.sendMessage(ChatColor.translateAlternateColorCodes('&', all));
						}

					} else {

						if (p.getItemInHand().hasItemMeta() && p.getItemInHand().getItemMeta().hasLore()) {
							
							Core.get.usingItemLoreTag.add(p);
							
						//	Inventory inv = Bukkit.createInventory(null, 45, ChatColor.translateAlternateColorCodes('&', "&6&lLore Remover"));
							
							ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 3);
							ItemMeta ismeta = is.getItemMeta();
							
							ArrayList<String> islore = (ArrayList<String>) ismeta.getLore();
							
							int index = 0;
							
							while (index <= islore.size()) {
								
								ismeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&b" + index));
								index++;
								
								for (String lores : islore) {
									
									islore.add(lores);
								}
							}
							

							
							if (p.getItemInHand().getAmount() > 1) {

								p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
								p.updateInventory();
							}

							if (p.getItemInHand().getAmount() == 1) {

								p.setItemInHand(null);
								p.updateInventory();
							}
							
						}

					}

				}

			} else {

				return;
			}

		} else {

			return;
		}

	}

}
