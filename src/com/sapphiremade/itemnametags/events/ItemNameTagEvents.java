package com.sapphiremade.itemnametags.events;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.sapphiremade.itemnametags.Core;
import com.sapphiremade.itemnametags.NameTags;

@SuppressWarnings("deprecation")
public class ItemNameTagEvents implements Listener {

	NameTags tags = new NameTags();

	@EventHandler
	public void onItemRename(AsyncPlayerChatEvent e) {

		Player p = e.getPlayer();

		if (Core.get.usingItemNameTag.contains(p)) {
			
			e.setCancelled(true);
			
			if (p.getItemInHand().getType() == Material.AIR || p.getItemInHand().getType() == null || p.getItemInHand() == null) {
				
				for (String all : Core.get.getConfig().getStringList("ItemNameTag.Messages.Air")) {

					p.sendMessage(ChatColor.translateAlternateColorCodes('&', all));
				}
				
			} else {
				
				String msg = e.getMessage();
				
				ItemStack is = p.getItemInHand();
				ItemMeta ismeta = is.getItemMeta();
				ismeta.setDisplayName(msg.replace("&", "�"));
				is.setItemMeta(ismeta);
				p.updateInventory();
				
				Core.get.usingItemNameTag.remove(p);
				
				for (String all : Core.get.getConfig().getStringList("ItemNameTag.Messages.ItemRenamed")) {

					p.sendMessage(ChatColor.translateAlternateColorCodes('&', all).replace("{ITEM}", is.getType().toString()).replace("{NEWNAME}", e.getMessage()));
				}
			}

		}

	}

	@EventHandler
	public void onPlayerCancelRename(AsyncPlayerChatEvent e) {

		Player p = e.getPlayer();

		String cancelWorld = Core.get.getConfig().getString("CancelWord").toLowerCase();

		if (Core.get.usingItemNameTag.contains(p)) {

			e.setCancelled(true);
			
			if (e.getMessage().equalsIgnoreCase(cancelWorld)) {

				Core.get.usingItemNameTag.remove(p);

				for (String all : Core.get.getConfig().getStringList("ItemNameTag.Messages.Cancel")) {

					p.sendMessage(ChatColor.translateAlternateColorCodes('&', all));
				}

				p.getInventory().addItem(tags.itemNameTag());
			}

		}

	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {

		Player p = e.getPlayer();

		if (Core.get.usingItemNameTag.contains(p)) {

			p.getInventory().addItem(tags.itemNameTag());
		}
	}

	@EventHandler
	public void onItemNameTagUse(PlayerInteractEvent e) {

		Player p = e.getPlayer();

		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {

			if (p.getItemInHand().isSimilar(tags.itemNameTag())) {

				if (Core.get.usingItemLoreTag.contains(p) || Core.get.usingItemNameTag.contains(p) || Core.get.usingLoreRemoveTag.contains(p)) {

					for (String all : Core.get.getConfig().getStringList("ItemNameTag.Messages.AlreadyNaming")) {

						p.sendMessage(ChatColor.translateAlternateColorCodes('&', all));
					}

				} else {

					Core.get.usingItemNameTag.add(p);

					for (String all : Core.get.getConfig().getStringList("ItemNameTag.Messages.Renaming")) {

						p.sendMessage(ChatColor.translateAlternateColorCodes('&', all));
					}

					if (p.getItemInHand().getAmount() > 1) {

						p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
						p.updateInventory();
					}

					if (p.getItemInHand().getAmount() == 1) {

						p.setItemInHand(null);
						p.updateInventory();
					}
				}

			} else {

				return;
			}

		} else {

			return;
		}

	}

}
