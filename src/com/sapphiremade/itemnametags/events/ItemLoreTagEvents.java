package com.sapphiremade.itemnametags.events;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.sapphiremade.itemnametags.Core;
import com.sapphiremade.itemnametags.NameTags;

@SuppressWarnings("deprecation")
public class ItemLoreTagEvents implements Listener {

	NameTags tags = new NameTags();

	@EventHandler
	public void onItemLoreAdd(AsyncPlayerChatEvent e) {

		Player p = e.getPlayer();

		if (Core.get.usingItemLoreTag.contains(p)) {

			e.setCancelled(true);

			if (p.getItemInHand().getType() == Material.AIR || p.getItemInHand().getType() == null || p.getItemInHand() == null) {

				for (String all : Core.get.getConfig().getStringList("ItemLoreTag.Messages.Air")) {

					p.sendMessage(ChatColor.translateAlternateColorCodes('&', all));
				}

			} else {

				ItemStack is = p.getItemInHand();
				ItemMeta ismeta = is.getItemMeta();

				if (!ismeta.hasLore()) {
						
					ArrayList<String> islore = new ArrayList<String>();
					islore.add(e.getMessage().replace("&", "�"));
					ismeta.setLore(islore);
					is.setItemMeta(ismeta);
					p.updateInventory();
				
				} else {
					
					ArrayList<String> islore = (ArrayList<String>) ismeta.getLore();
					islore.add(e.getMessage().replace("&", "�"));
					ismeta.setLore(islore);
					is.setItemMeta(ismeta);
					p.updateInventory();
				}

				Core.get.usingItemLoreTag.remove(p);

				for (String all : Core.get.getConfig().getStringList("ItemLoreTag.Messages.ItemRenamed")) {

					p.sendMessage(ChatColor.translateAlternateColorCodes('&', all).replace("{ITEM}", is.getType().toString()).replace("{ADDEDLORE}", "\n" + e.getMessage()));
				}
			}

		}

	}

	@EventHandler
	public void onPlayerCancelRename(AsyncPlayerChatEvent e) {

		Player p = e.getPlayer();

		String cancelWorld = Core.get.getConfig().getString("CancelWord").toLowerCase();

		if (Core.get.usingItemLoreTag.contains(p)) {

			e.setCancelled(true);

			if (e.getMessage().equalsIgnoreCase(cancelWorld)) {

				Core.get.usingItemLoreTag.remove(p);

				for (String all : Core.get.getConfig().getStringList("ItemLoreTag.Messages.Cancel")) {

					p.sendMessage(ChatColor.translateAlternateColorCodes('&', all));
				}

				p.getInventory().addItem(tags.itemLoreTag());
			}

		}

	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {

		Player p = e.getPlayer();

		if (Core.get.usingItemLoreTag.contains(p)) {

			p.getInventory().addItem(tags.itemLoreTag());
		}
	}

	@EventHandler
	public void onItemLoreTagUse(PlayerInteractEvent e) {

		Player p = e.getPlayer();

		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {

			if (p.getItemInHand().isSimilar(tags.itemLoreTag())) {

				if (Core.get.usingItemLoreTag.contains(p) || Core.get.usingItemNameTag.contains(p) || Core.get.usingLoreRemoveTag.contains(p)) {

					for (String all : Core.get.getConfig().getStringList("ItemLoreTag.Messages.AlreadyNaming")) {

						p.sendMessage(ChatColor.translateAlternateColorCodes('&', all));
					}

				} else {

					Core.get.usingItemLoreTag.add(p);

					for (String all : Core.get.getConfig().getStringList("ItemLoreTag.Messages.Renaming")) {

						p.sendMessage(ChatColor.translateAlternateColorCodes('&', all));
					}

					if (p.getItemInHand().getAmount() > 1) {

						p.getItemInHand().setAmount(p.getItemInHand().getAmount() - 1);
						p.updateInventory();
					}

					if (p.getItemInHand().getAmount() == 1) {

						p.setItemInHand(null);
						p.updateInventory();
					}
				}

			} else {

				return;
			}

		} else {

			return;
		}

	}

}
