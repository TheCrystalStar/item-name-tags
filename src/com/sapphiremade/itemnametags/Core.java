package com.sapphiremade.itemnametags;

import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.sapphiremade.itemnametags.events.ItemLoreTagEvents;
import com.sapphiremade.itemnametags.events.ItemNameTagEvents;

public class Core extends JavaPlugin {

	public static Core get;

	public ArrayList<Player> usingItemNameTag = new ArrayList<Player>();
	public ArrayList<Player> usingItemLoreTag = new ArrayList<Player>();
	public ArrayList<Player> usingLoreRemoveTag = new ArrayList<Player>();

	public static boolean checkPerm(Player p, String perm) {

		boolean hasPerm = false;

		if (!p.hasPermission(perm)) {
			hasPerm = false;
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou do not have permission to use that command!"));
		} else {
			hasPerm = true;
		}
		return hasPerm;
	}

	@Override
	public void onEnable() {

		get = this;

		try {

			MCUpdate update = new MCUpdate(this, true);

			Bukkit.getLogger().info("MCUpdate enabled and loaded");

		} catch (IOException e) {

			Bukkit.getLogger().info("Failed initialize MCUpdate");

		}

		getConfig().options().copyDefaults(true);
		saveDefaultConfig();
		
		registerEvents();

		getCommand("ItemNameTag").setExecutor(new Command());
	}

	private void registerEvents() {

		PluginManager pm = Bukkit.getServer().getPluginManager();

		pm.registerEvents(new ItemNameTagEvents(), this);
		pm.registerEvents(new ItemLoreTagEvents(), this);
	}

	@Override
	public void onDisable() {

		get = null;
	}

}
