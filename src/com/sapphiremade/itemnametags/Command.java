package com.sapphiremade.itemnametags;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command implements CommandExecutor {

	NameTags tags = new NameTags();

	private boolean isInt(String number) {

		try {

			Integer.parseInt(number);

		} catch (NumberFormatException e) {

			return false;
		}

		return true;
	}

	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args) {

		if (sender instanceof Player) {

			Player p = (Player) sender;

			if (args.length == 0) {

				if (Core.checkPerm(p, "ItemNameTags.Command") == false)
					return false;

				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b____________.[ &eItemTags &b].____________"));
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/tag list &7- List all the current type of tags."));
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/tag self <tag> &7- Give yourself that tag."));
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/tag give <player> <tag> <#> &7- Give a player x amount of that tag."));
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/tag giveall <tag> <#> &7- Giveall online users x amount of that tag."));
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));

				return true;
			}

			if (args.length == 1) {

				if (args[0].equalsIgnoreCase("list")) {

					if (Core.checkPerm(p, "ItemNameTags.Command.List") == false)
						return false;

					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b____________.[ &eItemTags &b].____________"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&l* &eNameTag &7- Allows users to rename the 'display' name of their item."));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&l* &eLoreTag &7- Allows users to add a line of lore to their item."));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
				}

				else if (args[0].equalsIgnoreCase("self")) {

					if (Core.checkPerm(p, "ItemNameTags.Command.Self") == false)
						return false;

					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b____________.[ &eItemTags &b].____________"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/tag self &c<tag>"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
				}

				else if (args[0].equalsIgnoreCase("give")) {

					if (Core.checkPerm(p, "ItemNameTags.Command.Give") == false)
						return false;

					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b____________.[ &eItemTags &b].____________"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/tag give &c<player> <tag> <#>"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
				}

				else if (args[0].equalsIgnoreCase("giveall")) {

					if (Core.checkPerm(p, "ItemNameTags.Command.Giveall") == false)
						return false;

					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b____________.[ &eItemTags &b].____________"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/tag giveall &c<tag> <#>"));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
				}

				return true;
			}

			if (args.length == 2) {

				if (args[0].equalsIgnoreCase("self")) {

					if (Core.checkPerm(p, "ItemNameTags.Command.Self") == false)
						return false;

					if (args[1].equalsIgnoreCase("nametag")) {

						p.getInventory().addItem(tags.itemNameTag());
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&bAdded x1 ItemNameTag to your inventory!"));

					} else if (args[1].equalsIgnoreCase("loretag")) {

						p.getInventory().addItem(tags.itemLoreTag());
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&bAdded x1 ItemLoreTag to your inventory!"));

					} else {

						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b____________.[ &eItemTags &b].____________"));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&l* &eNameTag &7- Allows users to rename the 'display' name of their item."));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&l* &eLoreTag &7- Allows users to add a line of lore to their item."));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));

					}
				}

				else if (args[0].equalsIgnoreCase("give")) {

					if (Core.checkPerm(p, "ItemNameTags.Command.Give") == false)
						return false;

					Player target = Bukkit.getPlayerExact(args[1]);

					if (target != null) {

						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b____________.[ &eItemTags &b].____________"));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/tag give " + args[1] + " &c<tag> <#>"));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));

					} else {

						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cThat player is currently offline!"));
					}

				}

				else if (args[0].equalsIgnoreCase("giveall")) {

					if (Core.checkPerm(p, "ItemNameTags.Command.Giveall") == false)
						return false;

					if (args[1].equalsIgnoreCase("nametag")) {

						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b____________.[ &eItemTags &b].____________"));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/tag giveall nametag &c<#>"));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));

					} else if (args[1].equalsIgnoreCase("loretag")) {

						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b____________.[ &eItemTags &b].____________"));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/tag giveall loretag &c<#>"));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));

					} else {

						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b____________.[ &eItemTags &b].____________"));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&l* &eNameTag &7- Allows users to rename the 'display' name of their item."));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&l* &eLoreTag &7- Allows users to add a line of lore to their item."));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));

					}
				}

				return true;
			}

			if (args.length == 3) {

				if (args[0].equalsIgnoreCase("giveall")) {

					if (Core.checkPerm(p, "ItemNameTags.Command.Giveall") == false)
						return false;

					if (args[1].equalsIgnoreCase("nametag")) {

						if (isInt(args[2])) {

							int xNumber = Integer.parseInt(args[2]);

							for (Player all : Bukkit.getOnlinePlayers()) {

								for (int i = 0; i < xNumber; i++) {

									all.getInventory().addItem(tags.itemNameTag());
								}

								all.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou received x" + xNumber + " of ItemNameTag(s) from &b " + p.getName()));
							}

							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou gave x" + xNumber + " of ItemNameTag(s) to all online players!"));

						} else {

							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlease enter a valid number!"));
						}

					} else if (args[1].equalsIgnoreCase("loretag")) {

						if (isInt(args[2])) {

							int xNumber = Integer.parseInt(args[2]);

							for (Player all : Bukkit.getOnlinePlayers()) {

								for (int i = 0; i < xNumber; i++) {

									all.getInventory().addItem(tags.itemLoreTag());
								}

								all.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou received x" + xNumber + " of ItemLoreTag(s) from &b " + p.getName()));
							}

							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou gave x" + xNumber + " of ItemLoreTag(s) to all online players!"));

						} else {

							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlease enter a valid number!"));
						}

					} else {

						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b____________.[ &eItemTags &b].____________"));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&l* &eNameTag &7- Allows users to rename the 'display' name of their item."));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&l* &eLoreTag &7- Allows users to add a line of lore to their item."));
						p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));

					}
				}

				else if (args[0].equalsIgnoreCase("give")) {

					if (Core.checkPerm(p, "ItemNameTags.Command.Give") == false)
						return false;

					Player target = Bukkit.getPlayerExact(args[1]);

					if (target != null) {

						if (args[2].equalsIgnoreCase("nametag")) {

							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b____________.[ &eItemTags &b].____________"));
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/tag give " + args[1] + " nametag &c<#>"));
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));

						} else if (args[2].equalsIgnoreCase("loretag")) {

							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b____________.[ &eItemTags &b].____________"));
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/tag give " + args[1] + " loretag &c<#>"));
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
						}

					} else {

						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cThat player is currently offline!"));
					}

				}

				return true;
			}

			if (args.length == 4) {

				if (args[0].equalsIgnoreCase("give")) {

					if (Core.checkPerm(p, "ItemNameTags.Command.Give") == false)
						return false;

					Player target = Bukkit.getPlayerExact(args[1]);

					if (target != null) {

						if (args[2].equalsIgnoreCase("nametag")) {

							if (isInt(args[3])) {

								int xNumber = Integer.parseInt(args[3]);

								for (int i = 0; i < xNumber; i++) {

									target.getInventory().addItem(tags.itemNameTag());
									
								}
								
								target.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou received x" + xNumber + " of ItemNameTag(s) from &b " + p.getName()));
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou gave x" + xNumber + " of ItemNameTag(s) to " + args[1]));

							} else {

								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlease enter a valid number!"));
							}

						} else if (args[2].equalsIgnoreCase("loretag")) {

							if (isInt(args[3])) {

								int xNumber = Integer.parseInt(args[3]);

								for (int i = 0; i < xNumber; i++) {

									target.getInventory().addItem(tags.itemLoreTag());
									
								}
								
								target.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou received x" + xNumber + " of ItemLoreTag(s) from &b " + p.getName()));
								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou gave x" + xNumber + " of ItemLoreTag(s) to " + args[1]));

							} else {

								p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlease enter a valid number!"));
							}
						}

					} else {

						p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cThat player is currently offline!"));
					}

				}

				return true;
			}

		} else {
			
			if (args.length == 3) {

				if (args[0].equalsIgnoreCase("giveall")) {

					if (args[1].equalsIgnoreCase("nametag")) {

						if (isInt(args[2])) {

							int xNumber = Integer.parseInt(args[2]);

							for (Player all : Bukkit.getOnlinePlayers()) {

								for (int i = 0; i < xNumber; i++) {

									all.getInventory().addItem(tags.itemNameTag());
								}

								all.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou received x" + xNumber + " of ItemNameTag(s) from &b " + sender.getName()));
							}

							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou gave x" + xNumber + " of ItemNameTag(s) to all online players!"));

						} else {

							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlease enter a valid number!"));
						}

					} else if (args[1].equalsIgnoreCase("loretag")) {

						if (isInt(args[2])) {

							int xNumber = Integer.parseInt(args[2]);

							for (Player all : Bukkit.getOnlinePlayers()) {

								for (int i = 0; i < xNumber; i++) {

									all.getInventory().addItem(tags.itemLoreTag());
								}

								all.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou received x" + xNumber + " of ItemLoreTag(s) from &b " + sender.getName()));
							}

							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou gave x" + xNumber + " of ItemLoreTag(s) to all online players!"));

						} else {

							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlease enter a valid number!"));
						}

					} else {

						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b____________.[ &eItemTags &b].____________"));
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&l* &eNameTag &7- Allows users to rename the 'display' name of their item."));
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&6&l* &eLoreTag &7- Allows users to add a line of lore to their item."));
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));

					}
				}

				else if (args[0].equalsIgnoreCase("give")) {

					Player target = Bukkit.getPlayerExact(args[1]);

					if (target != null) {

						if (args[2].equalsIgnoreCase("nametag")) {

							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b____________.[ &eItemTags &b].____________"));
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/tag give " + args[1] + " nametag &c<#>"));
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));

						} else if (args[2].equalsIgnoreCase("loretag")) {

							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b____________.[ &eItemTags &b].____________"));
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e/tag give " + args[1] + " loretag &c<#>"));
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ""));
						}

					} else {

						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cThat player is currently offline!"));
					}

				}

				return true;
			}

			if (args.length == 4) {

				if (args[0].equalsIgnoreCase("give")) {

					Player target = Bukkit.getPlayerExact(args[1]);

					if (target != null) {

						if (args[2].equalsIgnoreCase("nametag")) {

							if (isInt(args[3])) {

								int xNumber = Integer.parseInt(args[3]);

								for (int i = 0; i < xNumber; i++) {

									target.getInventory().addItem(tags.itemNameTag());
									
								}
								
								target.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou received x" + xNumber + " of ItemNameTag(s) from &b " + sender.getName()));
								sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou gave x" + xNumber + " of ItemNameTag(s) to " + args[1]));

							} else {

								sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlease enter a valid number!"));
							}

						} else if (args[2].equalsIgnoreCase("loretag")) {

							if (isInt(args[3])) {

								int xNumber = Integer.parseInt(args[3]);

								for (int i = 0; i < xNumber; i++) {

									target.getInventory().addItem(tags.itemLoreTag());
									
								}
								
								target.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou received x" + xNumber + " of ItemLoreTag(s) from &b " + sender.getName()));
								sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aYou gave x" + xNumber + " of ItemLoreTag(s) to " + args[1]));

							} else {

								sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cPlease enter a valid number!"));
							}
						}

					} else {

						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cThat player is currently offline!"));
					}

				}

				return true;
			}
		}

		return true;
	}

}
